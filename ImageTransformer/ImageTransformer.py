import numpy as np
import cv2
from config.Configuration import config

class ImageTransformer(object):

    def __init__(self):
        # src = np.array([[454, 174], [2750, 182], [3002, 2102], [205, 2101]], dtype="float32")
        # dst = np.array([[0, 0], [799,0], [799, 599], [0, 599]], dtype="float32")
        calibration = config.parser["CALIBRATION"]
        resolution = config.parser["RESOLUTION"]

        self.src = np.array([[calibration.getint("top_left_x"), calibration.getint("top_left_y")],
                            [calibration.getint("top_right_x"), calibration.getint("top_right_y")],
                            [calibration.getint("bottom_right_x"), calibration.getint("bottom_right_y")],
                            [calibration.getint("bottom_left_x"), calibration.getint("bottom_left_y")]],
                            dtype="float32")
        self.dst = np.array([[0, 0],
                             [resolution.getint("x") - 1, 0],
                             [resolution.getint("x") - 1, resolution.getint("y") - 1],
                             [0, resolution.getint("y") - 1]],
                            dtype="float32")

        self.resolution = resolution

    def transform(self, img):
        mat = cv2.getPerspectiveTransform(self.src, self.dst)
        return cv2.warpPerspective(img, mat, (self.resolution.getint("x"), self.resolution.getint("y")))

