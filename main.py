from pymouse import PyMouse

from ImageTransformer.ImageTransformer import ImageTransformer
from LaserDotDetector.LaserDotDetector import LaserDotDetector
from PositionCalculator.PositionCalculator import PositionCalculator
from config.Configuration import config # Do not remove this import
from picamera import PiCamera
from picamera.array import PiRGBArray
import cv2


def calibrate_screen():
    # TODO: calibrate
    print("Screen calibration not supported yet")
    pass


def main():

    # Variables
    transformer = ImageTransformer()
    print("Initialized ImageTransformer")
    detector = LaserDotDetector()
    print("Initialized LaserDotDetector")
    calculator = PositionCalculator()
    print("Initialized PositionCalculator")
    camera = PiCamera()
    print("Initialized PiCamera")
    mouse = PyMouse()
    print("Initialized PyMouse")
    rawCapture = PiRGBArray(camera)
    print("Initialized PiRGBArray") # ,size=(1024, 768))

    # Calibrate
    calibrate_screen()

    # Set resolution & framerate
    camera.resolution = (config.parser["CAMERA"].getint("resolution_x"), config.parser["CAMERA"].getint("resolution_y"))
    print("Camera resolution: " + str(camera.resolution[0]) + " x " + str(camera.resolution[1]))
    camera.framerate = config.parser["CAMERA"].getint("framerate")
    print("Camera framerate: " + str(camera.framerate))

    #Loop
    print("Starting capture")
    for frame in camera.capture_continuous(rawCapture, format = "bgr", use_video_port=True):
        captured_image = frame.array
        trans_img = transformer.transform(captured_image)
        contour_img = detector.detect(trans_img)
        x, y = calculator.calculate(contour_img)
        cv2.imshow("frame", captured_image)
        key = cv2.waitKey(1) & 0xFF
        rawCapture.truncate(0)

        if x < 0 and y < 0:
            print("Laser not detected")
            continue
        print("Detected laser at position: x=" + str(x) + " y=" + str(y))
        mouse.move(x, y)


if __name__ == "__main__":
    main()
