import cv2
import numpy as np

class LaserDotDetector:
    def __init__(self):
        # The first range did not detect laser dot in any of the 5 test images
        # self.first_red_hue_range_lower = np.array([0, 100, 100])
        # self.first_red_hue_range_upper = np.array([10, 255, 255])

        self.redHSVLowerRange = np.array([160, 100, 180])
        self.redHSVUpperRange = np.array([179, 255, 255])

        self.gaussianBlurKernelSize = (3, 3)
        self.gaussianBlurKernelSigmaX = 0
        self.gaussianBlurKernelSigmaY = 0

    def detect(self, img):
        imgHSV = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        imgHSV = cv2.GaussianBlur(imgHSV, self.gaussianBlurKernelSize,
                                   self.gaussianBlurKernelSigmaX, self.gaussianBlurKernelSigmaY)

        imgRed = cv2.inRange(imgHSV, self.redHSVLowerRange, self.redHSVUpperRange)
        return imgRed