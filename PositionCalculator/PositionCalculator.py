import cv2
import numpy as np

class PositionCalculator(object):
    def __init__(self):
        pass

    def calculate(self, img):
        res = cv2.findContours(img, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_SIMPLE)
        contours = res[1]
        if len(contours) == 0:
            return -1, -1
        M = cv2.moments(np.concatenate(contours))
        if M["m00"] == 0:
            return -2, -2
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        return cX, cY

